import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '../../services/app.service';
import { HttpClient } from '@angular/common/http';
import {HomeComponent} from '../home/home.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user = {
    name: String,
    email: String,
    password: String
  };

  private Url = 'http://localhost:8000/api/users';

  private  gurl = 'http://localhost:8000/api/getusers';


  constructor(private appService: AppService , private http: HttpClient , private router: Router ) { }

  login(e) {
    e.preventDefault();
    const email = e.target.elements[0].value;
    const password = e.target.elements[1].value;
    console.log(email, password);

      this.user.email = email;
      this.user.password = password;

      if (email.length === 0 || password.length === 0) {
        alert(' Please enter Email and Password');
      } else {
        const req = this.http.post(this.gurl, this.user)
          .subscribe(res => {
              if (Object.keys(res).length === 0 ) {
                console.log(res);
                alert('Please enter correct Email or Password / SignUp');
              } else {
                this.router.navigate(['']);
              }
            },
            err => {
              console.log('Error occured');
            }
          );
      }
    }

  createUser(ev) {
    ev.preventDefault();
    const name = ev.target.elements[0].value;
    const email = ev.target.elements[1].value;
    const password = ev.target.elements[2].value;
    const cpassword = ev.target.elements[3].value;

    if (name.length === 0 || email.length === 0 || password.length === 0) {
      alert(' Please provide all the information');
    } else if (password !== cpassword) {
      alert('Confirm Password does not match with Password');
    } else {
      this.user.name = name;
      this.user.email = email;
      this.user.password = password;
      console.log(this.user);

      const req = this.http.post(this.Url, this.user)
        .subscribe(res => {
          console.log(res);
          },
          err => {
            console.log('Error occured');
          }
        );
    }
  }


  ngOnInit() {
  }

  loginFacebook() {
    this.appService.loginFacebook();
  }

  loginTwitter() {
    this.appService.loginTwitter();
  }

  loginGoogle() {
    this.appService.loginGoogle();
  }

  loginGithub() {
    this.appService.loginGithub();
  }

}
