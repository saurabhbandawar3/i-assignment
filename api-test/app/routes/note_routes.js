module.exports = function (app, db) {
    app.post('/api/users', (req, res) => {
        //console.log(req);
        const user = { name: req.body.name, email: req.body.email, password: req.body.password };
        console.log(user);
        db.collection('users').insert(user, (err, result) => {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send(result.ops[0]);
            }
        });
    });

    app.post('/api/getusers', (req, res) => {
        console.log(req.body);
        const user = { email: req.body.email, password: req.body.password };
        db.collection('users').find({ 'email': user.email, 'password': user.password },{"_id":0}).toArray(function (err, item){
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send(item);
            }
        });
    });

};